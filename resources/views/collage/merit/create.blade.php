@extends('layouts.user.master')
@section('title','College Course')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            College Merit
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Course</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add merit</h3>
                    </div>
                    <form id="myform" method="post">
                        <div class="form-group">
                            <label>Course</label>
                            <select class="form-control" name="course_id">
                                <option>Select Course</option>
                                @foreach($course as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Merit Round</label>
                            <select class="form-control" name="merit_round_id" id="merit_round_id">
                                <option>Select Round no</option>
                                @foreach($merit_round as $m)
                                <option value="{{$m->id}}">{{$m->round_no}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group ">
                            <label>merit</label>
                            <input type="text" class="form-control" name="merit" placeholder="Enter reserved seat">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>


                        <div class="row">
                            <div class="box-footer">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("collage.college_merit.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("collage.college_merit.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })
    //Store Year
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });


    $(document).ready(function() {
        $('select[name="course_id"]').on('change', function() {
            var id = $(this).val();
            if(id) {
                $.ajax({
                    url: '{{route("collage.college_merit.getround")}}',
                    type: "GET",
                    data:{id:id},
                    dataType: "json",
                    success:function(data) {                      
                        $('select[name="merit_round_id"]').empty();
                        $.each(data.data, function(key, value) {                        
                            $('select[name="merit_round_id"]').append('<option value="'+ value['round_no'] +'">'+ value['round_no'] +'</option>');
                        });
                    }
                });
            }else{
                $('select[name="merit_round_id"]').empty();
            }
        });
    });
</script>

@endpush