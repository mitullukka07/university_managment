<?php

namespace App\Providers;

use App\Interfaces\LoginRepositoryInterface;
use App\Interfaces\RegisterRepositoryInterface;
use App\Repository\LoginRepository;
use App\Repository\RegisterRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RegisterRepositoryInterface::class,RegisterRepository::class);
        $this->app->bind(LoginRepositoryInterface::class,LoginRepository::class);
        $this->app->bind(AdmissionConfirmInterface::class,AdmissionConfirmeRepository::class);
    }
}
