<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>UMS | Registration</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{ asset('asset/img/library_black.png') }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('asset/css/blue.css')}}">
    <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition register-page">
    <div class="register-box" style="width:500px">
        <div class="register-logo">
            <a href="../../index2.html"><b>SignUp</b></a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Register a new student</p>
            <form id="myform" method="post">
                <div class="form-group has-feedback">
                <label>Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter name">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>

                <div class="form-group has-feedback">
                <label>Mobile</label>
                    <input type="text" class="form-control" name="contact_no" placeholder="Mobile">
                    <span class="glyphicon glyphicon-mobile form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="radio">
                    <label>
                        <input type="radio"  name="gender" id="optionsRadios1" value="M" checked>
                        Male
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio"  name="gender" id="optionsRadios2" value="F">
                        Female
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio"  name="gender" id="optionsRadios3" value="O" >
                       Other
                    </label>
                </div>
                
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" name="address" placeholder="Enter Address"></textarea>
                </div>

                <div class="form-group has-feedback">
                <label>Aadhar Number</label>
                    <input type="text   " class="form-control" onblur="AadharValidate()" name="adhaar_card_no" id="adhaar_card_no" placeholder="Aadhar card">
                    <span id="adhaarError"></span>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

                <div class="form-group">
                    <label>Image</label>
                    <input type="file" class="form-control" name="image" id="image" placeholder="Enter category">
                </div>

                <div class="form-group">
                    <label>DOB</label>
                    <input type="date" class="form-control" name="dob" id="dob" >
                </div>

                <div class="row">                    
                    <div class="col-xs-4">
                        <button type="submit" id="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div>                
                </div>
            </form>
            <a href="{{route('student.login')}}" class="text-center">I already have a membership</a>
        </div>
    </div>
    <script src="{{asset('asset/js/jquery.min.js')}}"></script>
    <script src="{{asset('asset/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('asset/js/icheck.min.js')}}"></script>

</body>

</html>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
<script>
    function AadharValidate()
    {
        var  aadhar = document.getElementById("adhaar_card_no").value;
        var adharcardTwelveDigit = /^\d{12}$/;
        if (aadhar != '') {
            if (aadhar.match(adharcardTwelveDigit)) {
                return true;
            }else{
                $('#adhaarError').text('Please enter 12 digit');
                alert("Enter valid Aadhar Number");
                return false;
            }
        }    
    }

    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("student.register")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    toastr.success('Register Successfully');
                    window.location.href = '{{route("student.login")}}';
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })

   

</script>