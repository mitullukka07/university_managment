<?php

namespace App\Http\Controllers\University;

use App\DataTables\CollegesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Collage\Register\StoreRequest;
use App\Models\College;
use App\Repository\CollegeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CollegeController extends Controller
{
    public function __construct(CollegeRepository $collegeRepository)
    {
        $this->collegeRepository = $collegeRepository;
    }

    public function index(CollegesDataTable $collegsDataTable)
    {
        return  $collegsDataTable->render('admin.collage.index');
    }

    public function create()
    {
        return view('admin.collage.create');
    }

    public function store(StoreRequest $request)
    {
        $store = $this->collegeRepository->store($request);
        return response()->json($store);
    }

    public function destroy(Request $request)
    {
        $book = College::where('id', $request->id)->delete();
        return redirect()->route('admin.college.index')->with('delete', "Delete Successfully");
    }
}
