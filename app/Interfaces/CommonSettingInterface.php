<?php

    namespace App\Interfaces;

    interface CommonSettingInterface
    {
        public function store($array); 
        public function update($array);               
    }

?>