<?php

namespace App\Http\Controllers\Collage;

use App\DataTables\AdmissionDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdmissionStudentController extends Controller
{
    public function index(AdmissionDataTable $admissionDataTable)
    {
        $college = Auth::guard('collage')->user()->id;  
        return  $admissionDataTable->with('college_id',$college)->render('collage.student_admission.index');
    }
}
