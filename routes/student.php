<?php

use App\Http\Controllers\Student\Auth\RegisterController;
use App\Http\Controllers\Student\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Student\StudentController;
use App\Http\Controllers\Student\AdmissionController;
use App\Http\Controllers\Student\StudentMarkController;
use App\Http\Controllers\Student\AdmissionConfirmController;

Route::group(['namespace' => 'Auth'], function () {
    Route::get('register',[RegisterController::class,'showRegister']);
    Route::post('register',[RegisterController::class,'Register'])->name('register');
    Route::get('login',[LoginController::class,'showLogin']);
    Route::post('login',[LoginController::class,'attemptLogin'])->name('login');
    Route::post('logout',[LoginController::class,'logout'])->name('logout');
});


Route::group(['middleware' => 'auth:student'], function () {
    Route::get('/dashboard', function () {
        return view('student.dashboard');
    })->name('dashboard');


    Route::group(['prefix' => 'student'], function () {
        Route::get('profile', [StudentController::class, 'profile'])->name('student.profile');
        Route::post('profile', [StudentController::class, 'updateprofile']);
        Route::get('changepassword',[StudentController::class,'changepassword'])->name('changepassword');
        Route::post('changepassword',[StudentController::class,'updatepassword']);
    });


    Route::group(['prefix'=>'admission','as'=>'admission.'],function(){
        Route::get('create',[AdmissionController::class,'create'])->name('create');
        Route::post('store',[AdmissionController::class,'store'])->name('store');        
        Route::get('index',[AdmissionController::class,'index'])->name('index');
        Route::get('edit/{id}',[AdmissionController::class,'edit'])->name('edit');
        Route::post('update',[AdmissionController::class,'update'])->name('update');
        Route::post('destroy',[AdmissionController::class,'destroy'])->name('destroy');
    });


    Route::group(['prefix'=>'mark','as'=>'mark.'],function(){
        Route::get('create',[StudentMarkController::class,'create'])->name('create');
        Route::post('store',[StudentMarkController::class,'store'])->name('store');        
        Route::get('index',[StudentMarkController::class,'index'])->name('index');
        Route::get('edit',[StudentMarkController::class,'edit'])->name('edit');
        Route::post('update',[StudentMarkController::class,'update'])->name('update');
        Route::post('destroy',[StudentMarkController::class,'destroy'])->name('destroy');
    });

    Route::group(['prefix'=>'admission','as'=>'admission.'],function(){
        Route::get('create',[AdmissionController::class,'create'])->name('create');        
        Route::get('index',[AdmissionController::class,'index'])->name('index');
        Route::post('destroy',[AdmissionController::class,'destroy'])->name('destroy');
    });

    Route::group(['prefix'=>'admission_confirm','as'=>'admission_confirm.'],function(){
        Route::get('create',[AdmissionConfirmController::class,'create'])->name('create');
        Route::post('store',[AdmissionConfirmController::class,'store'])->name('store');        
        Route::get('index',[AdmissionConfirmController::class,'index'])->name('index');
        Route::get('edit',[AdmissionConfirmController::class,'edit'])->name('edit');
        Route::post('update',[AdmissionConfirmController::class,'update'])->name('update');
        Route::post('destroy',[AdmissionConfirmController::class,'destroy'])->name('destroy');
        Route::get('changeStatus',[AdmissionConfirmController::class,'changeStatus'])->name('changeStatus');        
    });    
});
?>
