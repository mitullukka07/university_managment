<?php

namespace App\Http\Controllers\University;

use App\DataTables\AdmissionDataTable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdmissionController extends Controller
{
    public function index(AdmissionDataTable $admissionDataTable)
    {
        return  $admissionDataTable->render('admin.admission.index');
    }
}
