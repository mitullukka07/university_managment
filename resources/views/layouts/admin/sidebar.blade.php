<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('asset/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::guard('admin')->user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>

      <li>
        <a href="{{route('admin.dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="{{route('admin.college.index')}}">
          <i class="fa fa-university"></i> <span>Add collage</span>
        </a>
      </li>
    
      <li>
        <a href="{{route('admin.student.index')}}">
          <i class="fa fa-user"></i> <span>Mng Student</span>
        </a>
      </li>

      <li>
        <a href="{{route('admin.merit.index')}}">
          <i class="fa fa-list"></i> <span>Merit</span>
        </a>
      </li>

      <li>
        <a href="{{route('admin.course.index')}}">
          <i class="fa fa-book"></i> <span>Mng Course</span>
        </a>
      </li>

      <li>
        <a href="{{route('admin.admission.index')}}">
          <i class="fa fa-book"></i> <span>Admission</span>
        </a>
      </li>

      <li>
        <a href="{{route('admin.common_setting.index')}}">
          <i class="fa fa-cog"></i> <span>CommonSetting</span>
        </a>
      </li>

      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Charts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
          <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
          <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
          <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
        </ul>
      </li> -->
    </ul>
  </section>
</aside>