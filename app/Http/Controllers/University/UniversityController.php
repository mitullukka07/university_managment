<?php

namespace App\Http\Controllers\University;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UniversityController extends Controller
{
    public function profile()
    {
        $admin = Auth::guard('admin')->user();
        return view('admin.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {
        $admin = Auth::guard('admin')->user();
        $admin->name = $request->name;
        $admin->save();
        session()->flash('update','Profile updated successfully');
        return redirect()->back();        
    }
}
