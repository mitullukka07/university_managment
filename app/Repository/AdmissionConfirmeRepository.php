<?php
namespace App\Repository;

use App\Interfaces\AdmissionConfirmInterface;
use App\Models\Addmission;
use App\Models\AddmissionConfirmations;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class  AdmissionConfirmeRepository implements AdmissionConfirmInterface
{
    public function store($array)
    {
        $user = User::where('id',Auth::user()->id)->first();
        $admission = Addmission::where('user_id',Auth()->user()->id)->first();
        $college_name = $admission->college->name;
        $admission = new AddmissionConfirmations;
        $admission->addmission_id = $array->admission_id;
        $admission->confirm_college_id = $array->college_id;
        $admission->confirm_round_id = $array->merit_round_id;
        $admission->confirm_merit = $array->merit;    
        $admission->confirmation_type = 'M';
        $admission->save();

        $data = ['title' => $admission,'college'=>$college_name];
        $user['to'] = $user->email;
        Mail::send('mails.mail', $data, function ($message) use ($user) {
            $message->to($user['to']);
            $message->subject('Hello'); 
        });

        return $admission;
    }    
}
