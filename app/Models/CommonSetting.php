<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommonSetting extends Authenticatable
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'subject_id','marks'
    ];

    public function Subject()
    {
        return $this->belongsTo(Subject::class,'subject_id');
    }
}
