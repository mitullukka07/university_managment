<?php

namespace App\DataTables;

use App\Models\CollegeCourse;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CollegeCourseDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";                
                $btn = '<a  data-id="' . $data->id . '"  class="edit btn btn-info btn-sm btnedit" ><i class="fa fa-edit"></i></a>&nbsp';                        
                $btn .='<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->editColumn('college_id',function($data){
                return $data->College->name;
            })
            ->editColumn('course_id',function($data){
                return $data->Course->name;
            })
            ->rawColumns(['action','college_id','course_id'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\CollegeCourse $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CollegeCourse $model)
    {
        return $model->where('college_id',Auth::guard('collage')->user()->id)->with('Course')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('collegecourse-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('id'),
            Column::make('college_id')->title('College'),
            Column::make('course_id')->title('Course')->name('Course.name'),           
            Column::make('reserved_seat'),
            Column::make('merit_seat'),
            Column::make('seat_no')->title('Total seat'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(300)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CollegeCourse_' . date('YmdHis');
    }
}
