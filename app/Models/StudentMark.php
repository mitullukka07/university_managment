<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentMark extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id','subject_id','total_mark','obtain_mark'
    ];

    public function Subject()
    {
        return $this->belongsTo(Subject::class,'subject_id','id');
    }

    public function commonsetting()
    {
        return $this->belongsTo(CommonSetting::class,'subject_id','id');
    }
    
}
