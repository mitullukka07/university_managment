<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ Auth::guard('student')->user()->image }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::guard('student')->user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      @php
      $data = DB::table('addmissions')->where('user_id',Auth::user()->id)->first();
      @endphp

      @if($data == NULL)
      <li>
        <a href="{{route('student.admission.create')}}">
          <i class="fa fa-user"></i><span>Admission</span>
        </a>
      </li>
      @else if($data != NULL)
      <li>
        <a href="{{route('student.admission.index')}}">
          <i class="fa fa-user"></i><span>Admission</span>
        </a>
      </li>
      @endif
      <li>
        <a href="{{route('student.mark.index')}}">
          <i class="fa fa-pen"></i><span>Marks</span>
        </a>
      </li>
    </ul>
  </section>
</aside>