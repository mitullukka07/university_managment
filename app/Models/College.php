<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class College extends Authenticatable
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name','email','contact_no','address','password','logo','status'
    ];

    public function getLogoAttribute($value)
    {
        return $value ? asset('storage/app/public' . '/' . $value) : NULL;
    }
}
