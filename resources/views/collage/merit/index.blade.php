@extends('layouts.user.master')
@section('title','CollegeMerit')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            College Merit List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Category</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Update College Course</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="editform">
                                        @csrf
                                        @php
                                        $course = DB::table('courses')->select('id','name')->get();
                                        @endphp
                                        <div class="form-group">
                                            <label>Course</label>
                                            <select class="form-control" name="course_id" id="course_id">
                                                <option>Select Course</option>
                                                @foreach($course as $c)
                                                <option value="{{$c->id}}">{{$c->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @php
                                          $merit_round = DB::table('merit_rounds')->select('id','round_no')->get();
                                        @endphp
                                        <div class="form-group">
                                            <label>Merit Round</label>
                                            <select class="form-control" name="merit_round_id" id="merit_round_id">
                                                <option>Select Round no</option>
                                                @foreach($merit_round as $m)
                                                <option value="{{$m->id}}">{{$m->round_no}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group ">
                                            <label>Merit</label>
                                            <input type="text" class="form-control" name="merit" id="merit" placeholder="Enter merit">
                                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        </div>
                                </div>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-body">
                        <a href="{{route('collage.college_merit.create')}}" id="update" name="update" style="float: right;margin-top:10px" class="btn btn-primary"><i class="fa fa-plus"></i>Add College Merit</a>
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection

@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).on("click", ".btnedit", function(e) {
        e.preventDefault();
        $('#myModal').modal('show');
        var id = $(this).attr("data-id");
        var url = "{{route('collage.college_merit.edit',':id')}}";
        url = url.replace(':id', id);
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#id").val(data.college_merit.id);
                $("#course_id").val(data.college_merit.course_id);
                $("#merit").val(data.college_merit.merit);
                $("#merit_round_id").val(data.college_merit.merit_round_id);
            },
        });
    });

    $(document).on("click", ".btndelete", function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('collage.college_merit.destroy')}}",
            type: 'post',
            data: {
                id: id
            },
            dataType: "JSON",
            success: function(data) {
                swal({
                    title: 'Delete',
                    text: 'Are you sure delete succesfully ??',
                    buttons: ['Cancel', 'Delete']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['collegemerit-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            }
        });
    });

    //Update
    $("#update").on('click', function(e) {
        e.preventDefault();
        var form = $("#editform")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route("collage.college_merit.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['collegemerit-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(key, value) {
                    $('input[name=' + key + ']').after('<span class="error" style="color:red">' + value + '</span>');
                });
            }
        });
    });

    $(document).ready(function() {
        $('select[name="course_id"]').on('change', function() {
            var id = $(this).val();
            if (id) {
                $.ajax({
                    url: '{{route("collage.college_merit.getround")}}',
                    type: "GET",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        $('select[name="merit_round_id"]').empty();
                        $.each(data.data, function(key, value) {
                            $('select[name="merit_round_id"]').append('<option value="' + value['round_no'] + '">' + value['round_no'] + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="merit_round_id"]').empty();
            }
        });
    });
</script>

@endpush