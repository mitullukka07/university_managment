@extends('layouts.admin.master')
@section('title','Category')
@section('content')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Category</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Category Add</h3>
          </div>
          <form id="myform">
            <div class="box-body">
              <div class="form-group">
                <label>Course</label>
                <input type="text" class="form-control" name="category" id="category" placeholder="Enter category">                
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script>
  $(document).ready(function() {
    $("#submit").click(function(e) {
      e.preventDefault();
      var form = $("#myform")[0];
      var data = new FormData(form);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{route("admin.category.store")}}',
        type: 'post',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function(result) {
          swal({
            title: "Inserted",
            text: "Insert Succesfully!",
            closeOnConfirm: false,
            closeOnCancel: false,
            buttons: ["Cancel", "Submit"]
          }).then(function(isConfirm) {
            if (isConfirm) {
              $('#myform')[0].reset();
              window.location.href = '{{route("admin.category.store")}}';
            } else {
              swal("Cancelled", "", "error")
            }
          });
        },
        error: function(result) {
          $.each(result.responseJSON.errors, function(i, error) {
            $('.' + i + 'error').empty();
            var el = $(document).find('[name="' + i + '"]');
            el.after($('<span class="'+i+'error" style="color: red;">' + error[0] + '</span>'));
          });
        }
      })
    });
  })
</script>

@endpush