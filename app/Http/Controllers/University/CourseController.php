<?php

namespace App\Http\Controllers\University;

use App\DataTables\CourseDataTable;
use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index(CourseDataTable $courseDataTable)
    {
        return  $courseDataTable->render('admin.course.index') ;     
    }

    public function edit($id)
    {
        $course = Course::find($id);
        return response()->json(['course'=>$course]);
    }

    public function update(Request $request)
    {
        $course = Course::find($request->id);
        $course->name = $request->name;
        if($course->save())
        {
            return response()->json(['course'=>$course]);
        }
    }
}
