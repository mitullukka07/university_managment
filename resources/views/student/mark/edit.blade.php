@extends('layouts.student.master')
@section('title','Marks')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Update 12th Marks
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Course</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Update Mark</h3>
                    </div>
                    <!-- <form id="myform" method="post"> -->
                    {{ Form::open(['id'=>'myform','method'=>'post','enctype'=>'multipart/form-data'])  }}

                    @foreach($student_mark as $cou)                        
                    <div class="form-group">                   
                        <label>{{$cou->Subject->name}}</label>                        
                        <input type="text" class="form-control" value="{{$cou->obtain_mark}}" id="{{$cou->id}}" name="subject_id[{{$cou->id}}]" id="subject_id" placeholder="Enter subject">
                    </div>
                    @endforeach
                    
                    <input type="hidden" value="{{Auth::guard('student')->user()->id}}" name="id">
                    <div class="row">
                        <div class="box-footer">
                            <button type="submit" id="update" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    <!-- </form> -->
                    {{ Form::close()   }}
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
     //Update
     $("#update").on('click', function(e) {
        e.preventDefault();
        var form = $("#myform")[0];
        var data = new FormData(form);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{route("student.mark.update")}}',
            data: data,
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function() {
                swal({
                    title: 'Updated',
                    text: 'Updated Succesfully',
                    buttons: ['Cancel', 'Update']
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        window.LaravelDataTables['collegecourse-table'].draw();
                        $("#myModal").modal('hide');
                    } else {
                        swal("Cancelled", "", "error");
                    }
                });
            },
            error: function(data) {
                $.each(data.responseJSON.errors, function(key, value) {
                    $('input[name=' + key + ']').after('<span class="error" style="color:red">' + value + '</span>');
                });
            }
        });
    });

    
</script>

@endpush