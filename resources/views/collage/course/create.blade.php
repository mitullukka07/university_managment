@extends('layouts.user.master')
@section('title','College Course')
@push('css')
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet" />
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            College Course
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Course</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Course</h3>
                    </div>
                    <form id="myform" method="post">                       
                        <!-- <div class="form-group">
                            <label>College</label>
                            <select class="form-control" name="college_id">
                                <option>Select College</option>
                                @foreach($college as $c)
                                <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div> -->

                        <div class="form-group">
                            <label>Course</label>
                            <select class="form-control" name="course_id">
                            <option>Select Course</option>
                                @foreach($course as $cou)
                                <option value="{{$cou->id}}">{{$cou->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- <div class="form-group ">
                            <label>Seat_No</label>
                            <input type="text" class="form-control" name="seat_no" placeholder="Enter seat no">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div> -->

                        <div class="form-group ">
                            <label>Reserve_Seat</label>
                            <input type="text" class="form-control" name="reserved_seat" placeholder="Enter reserved seat">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>

                        <div class="form-group ">
                            <label>Merit_Seat</label>
                            <input type="text" class="form-control" name="merit_seat" placeholder="Enter merit">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="box-footer">
                                <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function() {
        $("#submit").click(function(e) {
            e.preventDefault();
            var form = $("#myform")[0];
            var data = new FormData(form);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route("collage.college_course.store")}}',
                type: 'post',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(result) {
                    swal({
                        title: "Inserted",
                        text: "Insert Succesfully!",
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        buttons: ["Cancel", "Submit"]
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $('#myform')[0].reset();
                            window.location.href = '{{route("collage.college_course.index")}}';
                        } else {
                            swal("Cancelled", "", "error")
                        }
                    });
                },
                error: function(result) {
                    $.each(result.responseJSON.errors, function(i, error) {
                        $('.' + i + 'error').empty();
                        var el = $(document).find('[name="' + i + '"]');
                        el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
                    });
                }
            })
        });
    })
    //Store Year
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
</script>

@endpush