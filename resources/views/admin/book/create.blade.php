@extends('layouts.admin.master')
@section('title','Book')
@push('css')

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
@endpush
@section('content')


<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Book
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Book</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Book Add</h3>
          </div>
          <form id="myform" enctype="multipart/form-data">
            <div class="box-body">
              <div div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category_id">
                  <option>Select Category</option>
                  @foreach($category as $cat)
                  <option value="{{$cat->id}}">{{$cat->name}}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Enter title">
                <span class="text-danger" id="Errorcategory"></span></br>
              </div>

              <div class="form-group">
                <label>Author</label>
                <input type="text" class="form-control" name="author" id="author" placeholder="Enter author">
                <span class="text-danger" id="Errorcategory"></span></br>
              </div>



              <div class="form-group">
                <label>No_of_copy</label>
                <input type="text" class="form-control" name="no_of_copy" id="no_of_copy" placeholder="Enter No_of_copy">
                <span class="text-danger" id="Errorcategory"></span></br>
              </div>

              <div div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                  <option value="1">Available</option>
                  <option value="2">Not Available</option>
                </select>
              </div>

              <div class="form-group">
                <label>Publish_year</label>
                <input type="text" class="form-control" name="publish" id="datepicker" placeholder="Enter category">
                <span class="text-danger" id="Errorcategory"></span></br>
              </div>

              <div class="form-group">
                <label>Langauge</label>
                <input type="text" class="form-control" name="language" id="language" placeholder="Enter No_of_copy">
                <span class="text-danger" id="Errorcategory"></span></br>
              </div>

              <div class="form-group">
                <label>Cover_image</label>
                <input type="file" class="form-control" name="cover_image" id="cover_image" placeholder="Enter category">
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>

<script>
  $(document).ready(function() {
    $("#submit").click(function(e) {
      e.preventDefault();
      var form = $("#myform")[0];
      var data = new FormData(form);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{route("admin.book.store")}}',
        type: 'post',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function(result) {
          swal({
            title: "Inserted",
            text: "Insert Succesfully!",
            closeOnConfirm: false,
            closeOnCancel: false,
            buttons: ["Cancel", "Submit"]
          }).then(function(isConfirm) {
            if (isConfirm) {
              $('#myform')[0].reset();
              window.location.href = '{{route("admin.book.index")}}';
            } else {
              swal("Cancelled", "", "error")
            }
          });
        },
        error: function(result) {
          $.each(result.responseJSON.errors, function(i, error) {
            $('.' + i + 'error').empty();
            var el = $(document).find('[name="' + i + '"]');
            el.after($('<span class="' + i + 'error" style="color: red;">' + error[0] + '</span>'));
          });
        }
      })
    });
  })


  //Store Year

  $("#datepicker").datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years"
  });
</script>

@endpush