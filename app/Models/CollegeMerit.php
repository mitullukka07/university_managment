<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollegeMerit extends Authenticatable
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'college_id','course_id','merit_round_id','merit'
    ];

  
    public function Course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }

    public function Merit()
    {
        return $this->belongsTo(MeritRound::class,'merit_round_id','id');
    }
}
