<?php

namespace App\Http\Controllers\University;

use App\DataTables\MeritRoundDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\MeritRound\StoreRequest;
use App\Http\Requests\MeritRound\UpdateRequest;
use App\Models\Course;
use App\Models\MeritRound;
use Illuminate\Http\Request;

class MeritRoundController extends Controller
{
    public function index(MeritRoundDataTable $meritroundDataTable)
    {
        return  $meritroundDataTable->render('admin.merit.index') ;     
    }

    public function create()
    {
        $course = Course::select('id','name')->get();
        return view('admin.merit.create',compact('course'));
    }

    public function store(StoreRequest $request)
    {   
        $merit_round = new MeritRound;
        $merit_round->round_no = $request->round_no;
        $merit_round->course_id = $request->course_id;
        $merit_round->start_date = $request->fromdate;
        $merit_round->end_date = $request->todate;
        $merit_round->merit_result_declare_date = $request->resultdate;
        $merit_round->save();
        return response()->json('1');
    }


    public function edit($id)
    {
        $merit = MeritRound::find($id);
        return response()->json(['merit'=>$merit]);
    }

    public function update(UpdateRequest $request)
    {
        $merit_round = MeritRound::find($request->id);
        $merit_round->round_no = $request->round_no;
        $merit_round->course_id = $request->course_id;
        $merit_round->start_date = $request->fromDate;
        $merit_round->end_date = $request->toDate;
        $merit_round->merit_result_declare_date	 = $request->resultdate;
        $merit_round->save();        
        return response()->json(['property'=>$merit_round]);        
    }

    public function destroy(Request $request)
    {
        $merit = MeritRound::where('id',$request->id)->delete();
        return response()->json(1);
    }
}
