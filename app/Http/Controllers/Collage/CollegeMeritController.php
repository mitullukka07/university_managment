<?php

namespace App\Http\Controllers\Collage;

use App\DataTables\CollegeMeritDataTable;
use App\Http\Controllers\Controller;
use App\Models\College;
use App\Models\CollegeMerit;
use App\Models\Course;
use App\Models\MeritRound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CollegeMeritController extends Controller
{
    public function index(CollegeMeritDataTable $collegemeritDataTable)
    {
        return  $collegemeritDataTable->render('collage.merit.index') ;     
    }

    public function create()
    {
        $course = Course::select('id','name')->get();  
        $merit_round = MeritRound::select('id','round_no')->orderBy('id','DESC')->get();  
        return view('collage.merit.create',compact('course','merit_round'));
    }    

    public function store(Request $request)
    {
        $round_no = MeritRound::where('round_no',$request->merit_round_id)->where('course_id',$request->course_id)->pluck('id')->first();
        $merit = new CollegeMerit;
        $merit->college_id = request()->user()->id;
        $merit->course_id = $request->course_id;
        $merit->merit_round_id = $round_no;
        $merit->merit = $request->merit;
        $merit->save();
        return response()->json('1');
    }

    public function edit($id)
    {
        $college_merit = CollegeMerit::find($id);              
        return response()->json(['college_merit'=>$college_merit]);
    }

    public function update(Request $request)
    {
        $merit_round = CollegeMerit::find($request->id);
        $merit_round->course_id = $request->course_id;
        $merit_round->merit_round_id = $request->merit_round_id;
        $merit_round->merit = $request->merit;
        $merit_round->save();        
        return response()->json(['property'=>$merit_round]);
    }


    public function destroy(Request $request)
    {
        $collage_merit = CollegeMerit::where('id',$request->id)->delete();
        return response()->json(1);
    }

    public function getRound(Request $request)
    {
        dd($request->all());
        $data = MeritRound::select('round_no')->where('course_id',$request->id)->get();
        return response()->json(['data'=>$data]);
    }
}
