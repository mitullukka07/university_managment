<?php

namespace App\Http\Controllers\Student\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = "student/dashboard";
    public function __construct()
    {
        $this->middleware('guest:student')->except('logout');
    }

    public function showLogin()
    {
        return view('student.login');
    }

    protected function attemptLogin(Request $request)
    {        
        $credentials = $request->only('email', 'password');
        if (Auth::guard('student')->attempt($credentials)) {
            $businessData = User::where('email', $request->email);
            return redirect()->route('student.dashboard');
        }
        else
        {
            return redirect()->back()
            ->with('message','These credentials do not match our records
            ');
        }
    }

    public function logout(Request $request)
    {
        $this->guard('student')->logout();
        return  redirect()->route('student.login');
    }

    protected function guard()
    {
        return Auth::guard('student');
    }
}
