<?php

namespace App\Http\Controllers\Student\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Student\Register\StoreRequest;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function showRegister()
    {
        return view('student.register');
    }

    public function register(StoreRequest $request)
    {
        $image  = uploadFile($request->image, 'app/public');
        $student = new User;
        $student->name = $request->name;
        $student->email = $request->email;
        $student->contact_no = $request->contact_no;
        $student->password= Hash::make($request->password);
        $student->gender = $request->gender;
        $student->address = $request->address;
        $student->adhaar_card_no = $request->adhaar_card_no;
        $student->image = $image;   
        $student->dob = $request->dob;
        $student->save();
        return response()->json($student);
    }
}
