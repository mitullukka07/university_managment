<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    public function profile()
    {
        $admin = Auth::guard('student')->user();
        return view('student.profile',compact('admin'));
    }

    public function updateprofile(Request $request)
    {              
        $admin = Auth::guard('student')->user();
        if ($request->hasFile('image')) {
            $destination = 'app/public/' . $admin->image;
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $file->move('app/public/', $name);
            $admin->image = $name;
        }
        $admin->name = $request->name;
        $admin->address = $request->address;
        $admin->save();
        session()->flash('update','Profile updated successfully');
        return redirect()->back();
    }

    public function changepassword()
    {
        return view('student.change_password');
    }

    public function updatepassword(Request $request)
    {
        $hashedPassword = Auth::guard('student')->user()->password;
        if (Hash::check($request->oldpassword, $hashedPassword)) {
            $update = bcrypt($request->newpassword);
            $users = Auth::guard('student')->id();
            $id = User::where('id', $users)->update(['password' => $update]);
            if ($id) {
                session()->flash('update', 'password updated successfully');
            }
            return redirect()->back();
        } else {
            session()->flash('error', 'old password doesnt matched');
            return redirect()->back();
        }
    }

}
