<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addmission extends Model
{
    use HasFactory;

    protected $fillable = [
        'college_id','merit','user_id','addmission_date','addmission_code','status','course_id','merit_round_no'
    ];

    protected $casts = [
        'college_id' => 'array',
    ];

    public function User()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function Course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }

    public function collegeMerit()
    {
        return $this->hasOne(CollegeMerit::class);
    }

    public function college()
    {
        return $this->belongsTo(College::class,'college_id','id');
    }
    
}
