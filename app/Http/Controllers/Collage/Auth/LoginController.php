<?php

namespace App\Http\Controllers\Collage\Auth;

use App\Http\Controllers\Controller;
use App\Models\College;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = "collage/dashboard";

    public function __construct()
    {
        $this->middleware('guest:collage')->except('logout');
    }

    public function showLogin()
    {
        return view('collage.login');
    }

    protected function attemptLogin(Request $request)
    {

        $credentials = $request->only('email', 'password');
        if (Auth::guard('collage')->attempt($credentials)) {
            $businessData = College::where('email', $request->email);
            return redirect()->route('collage.dashboard');
        } else {
            return redirect()->back()
                ->with('message', 'These credentials do not match our records
            ');
        }
    }  

    public function logout(Request $request)
    { 
        $this->guard('collage')->logout();
        return  redirect()->route('collage.login');        
    }

    protected function guard()
    {
        return Auth::guard('collage');
    }
}
