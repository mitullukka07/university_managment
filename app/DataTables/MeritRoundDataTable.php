<?php

namespace App\DataTables;

use App\Models\MeritRound;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MeritRoundDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";                
                $btn = '<a  data-id="' . $data->id . '"  class="edit btn btn-info btn-sm btnedit" ><i class="fa fa-edit"></i></a>&nbsp';                        
                $btn .='<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->editColumn('status',function($data){
                if ($data->status == '1') {
                    return '<a class="btn btn-success btn-xs">Active</a>';
                } else {
                    return '<a class="btn btn-danger btn-xs">Inactive</a>';
                }
            })
            ->editColumn('course_id',function($data){
                return $data->Course->name;
            })
            ->rawColumns(['action','status','course_id'])
            ->addIndexColumn();;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MeritRound $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MeritRound $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('meritround-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('round_no'),
            Column::make('course_id')->title('Course Name'),
            Column::make('start_date'),
            Column::make('end_date'),
            Column::make('merit_result_declare_date'),
            Column::make('status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(300)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'MeritRound_' . date('YmdHis');
    }
}
