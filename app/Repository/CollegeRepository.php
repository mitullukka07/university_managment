<?php
namespace App\Repository;

use App\Interfaces\CollegeInterface;
use App\Interfaces\HomeRepositoryInterface;
use App\Models\College;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class  CollegeRepository implements CollegeInterface
{
    public function store($array)
    {
        $logo  = uploadFile($array->logo, 'app/public');
        $collage = new College;
        $collage->name = $array->name;
        $collage->email = $array->email;
        $collage->contact_no = $array->contact_no;
        $collage->address = $array->address;
        $collage->logo = $logo;
        $collage->password= Hash::make($array->password);
        $collage->status = '1';
        $collage->save();
        return $collage;
    }    
}
?>

