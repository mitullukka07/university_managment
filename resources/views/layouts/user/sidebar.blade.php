<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset('asset/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ Auth::guard('collage')->user()->name }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{route('collage.dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="{{route('collage.college_course.index')}}">
          <i class="fa fa-building"></i> <span>Mng College course</span>
        </a>
      </li>

      <li>
        <a href="{{ route('collage.admission.index') }}">
          <i class="fa fa-user"></i><span>Student Details</span>
        </a>
      </li>

      <li>
        <a href="{{ route('collage.reserved.index') }}">
          <i class="fa fa-user"></i><span>Reserve Quota Pending</span>
        </a>
      </li>

      <li>
        <a href="{{ route('collage.reserved.index2') }}">
          <i class="fa fa-user"></i><span>Reserve Quota Confirm</span>
        </a>
      </li>
      
      <li>
        <a href="{{route('collage.college_merit.index')}}">
          <i class="fa fa-plus"></i> <span>College merits</span>
        </a>
      </li>

    </ul>
  </section>
</aside>