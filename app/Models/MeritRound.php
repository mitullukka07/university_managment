<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeritRound extends Authenticatable
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'course_id','start_date','end_date','merit_result_declare_date'
    ];

    public function Course()
    {
        return $this->belongsTo(Course::class,'course_id','id');
    }
}
