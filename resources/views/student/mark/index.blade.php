@extends('layouts.student.master')
@section('title','Marks')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Marks List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Marks</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Update Marks</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="editform">
                                        @csrf
                                        @php
                                        $subject = DB::table('subjects')->select('id','name')->get();
                                        @endphp
                                        @foreach($subject as $cou)
                                        <div class="form-group">
                                            <label>{{$cou->name}}</label>
                                            <input type="text" class="form-control" name="subject_id[{{$cou->id}}]"  id="subject_id" placeholder="Enter subject">
                                        </div>
                                        @endforeach
                                        <div class="form-group ">
                                            <label>Total Mark</label>
                                            <input type="text" class="form-control" name="total_mark" id="total_mark" placeholder="Enter total mark">
                                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        </div>
                                </div>
                                <input type="hidden" id="id" name="id">
                                </form>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="update" name="update" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                    $subject1 = DB::table('student_marks')->where('user_id',Auth::guard('student')->user()->id)->count();
                    @endphp
                    <div class="box-body">
                        @if($subject1 == 0)
                            <a href="{{route('student.mark.create')}}" id="add" name="add" style="float: right;margin-top:10px" class="btn btn-primary"><i class="fa fa-plus"></i>Add Subject</a>
                        @else
                            <a href="{{route('student.mark.edit')}}" id="update" name="update" style="float: right;margin-top:10px" class="btn btn-primary"><i class="fa fa-pen"></i>Edit Subject</a>
                        @endif
                        {!! $dataTable->table(['class' => 'table table-bordered dt-responsive nowrap']) !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="control-sidebar-bg"></div>
</div>
@endsection
@push('js')
{!! $dataTable->scripts() !!}
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>

<script>
    

   
</script>

@endpush