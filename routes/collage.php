<?php

use App\Http\Controllers\Collage\Auth\RegisterController;
use App\Http\Controllers\Collage\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Collage\CollegeController;
use App\Http\Controllers\Collage\CollageCourseController;
use App\Http\Controllers\Collage\CollegeMeritController;
use App\Http\Controllers\Collage\AdmissionStudentController;

use App\Http\Controllers\Collage\ReservedQuotaController;

Route::group(['namespace' => 'Auth'], function () {
    # Login Routes
    Route::get('login',[LoginController::class,'showLogin']);
    Route::post('login',[LoginController::class,'attemptLogin'])->name('login');
    Route::post('logout',[LoginController::class,'logout'])->name('logout');
});

Route::group(['middleware' => 'auth:collage'], function () {
    Route::get('/dashboard', function () {
        return view('collage.dashboard');
    })->name('dashboard');

    Route::group(['prefix' => 'college'], function () {
        Route::get('profile', [CollegeController::class, 'profile'])->name('admin.profile');
        Route::post('profile', [CollegeController::class, 'updateprofile']);
        Route::get('changepassword',[CollegeController::class,'changepassword'])->name('changepassword');
        Route::post('changepassword',[CollegeController::class,'updatepassword']);
    });

    Route::group(['prefix'=>'college_course','as'=>'college_course.'],function(){
        Route::get('create',[CollageCourseController::class,'create'])->name('create');
        Route::post('store',[CollageCourseController::class,'store'])->name('store');        
        Route::get('index',[CollageCourseController::class,'index'])->name('index');
        Route::get('edit/{id}',[CollageCourseController::class,'edit'])->name('edit');
        Route::post('update',[CollageCourseController::class,'update'])->name('update');
        Route::post('destroy',[CollageCourseController::class,'destroy'])->name('destroy');
    });


    Route::group(['prefix'=>'college_merit','as'=>'college_merit.'],function(){
        Route::get('create',[CollegeMeritController::class,'create'])->name('create');
        Route::post('store',[CollegeMeritController::class,'store'])->name('store');        
        Route::get('index',[CollegeMeritController::class,'index'])->name('index');
        Route::get('edit/{id}',[CollegeMeritController::class,'edit'])->name('edit');
        Route::post('update',[CollegeMeritController::class,'update'])->name('update');
        Route::post('destroy',[CollegeMeritController::class,'destroy'])->name('destroy');
        Route::get('getRound',[CollegeMeritController::class,'getRound'])->name('getround');
    });

    Route::group(['prefix'=>'admission','as'=>'admission.'],function(){
        Route::get('create',[AdmissionStudentController::class,'create'])->name('create');        
        Route::get('index',[AdmissionStudentController::class,'index'])->name('index');
        Route::post('destroy',[AdmissionStudentController::class,'destroy'])->name('destroy');
    });

    Route::group(['prefix'=>'reserved','as'=>'reserved.'],function(){
        Route::get('index',[ReservedQuotaController::class,'index'])->name('index');
        Route::get('index2',[ReservedQuotaController::class,'index2'])->name('index2');
        Route::get('quotaAdmissionApprove',[ReservedQuotaController::class,'quotaAdmissionApprove'])->name('quotaAdmissionApprove');
    });

});
?>
