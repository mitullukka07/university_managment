<?php

namespace App\DataTables;

use App\Models\CommonSetting;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CommonSettingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $btn = "";                
                $btn = '<a  data-id="' . $data->id . '"  class="edit btn btn-info btn-sm btnedit" ><i class="fa fa-edit"></i></a>&nbsp';                        
                $btn .='<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })
            ->editColumn('subject_id',function($data){
                return $data->Subject->name;
            })
            ->rawColumns(['action','subject_id'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\CommonSetting $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CommonSetting $model)
    {
        return $model->newQuery()->with('Subject');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('commonsetting-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('subject_id')->title('Subject Name')->name('Subject.name'),
            Column::make('marks'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(300)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CommonSetting_' . date('YmdHis');
    }
}
