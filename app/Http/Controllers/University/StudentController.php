<?php

namespace App\Http\Controllers\University;

use App\DataTables\StudentDataTable;
use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(UserDataTable $userDataTable)
    {
        return  $userDataTable->render('admin.student.index') ;     
    }

    public function destroy(Request $request)
    {
        $student = User::where('id',$request->id)->delete();
        return response()->json(['student'=>$student]);
    }

}
