<?php

namespace App\DataTables;

use App\Models\CollegeMerit;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CollegeMeritDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($data) {
                $btn = "";
                $btn = '<a  data-id="' . $data->id . '"  class="edit btn btn-info btn-sm btnedit" ><i class="fa fa-edit"></i></a>&nbsp';
                $btn .= '<a  data-id="' . $data->id . '" class="edit btn btn-danger btn-sm btndelete "><i class="fa fa-trash"></i></a>';
                return $btn;
            })

            ->editColumn('course_id', function ($data) {
                return $data->Course->name;
            })
            ->editColumn('merit_round_id', function ($data) {
                return $data->Merit->round_no;
            })
            ->rawColumns(['action', 'merit_round_id', 'course_id'])
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\CollegeMerit $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CollegeMerit $model)
    {
        return $model->where('college_id',Auth::guard('collage')->user()->id)->with('Course')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('collegemerit-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            
            Column::make('No')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('course_id'),
            Column::make('merit_round_id')->title('Merit Round'),
            Column::make('merit'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(300)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CollegeMerit_' . date('YmdHis');
    }
}
